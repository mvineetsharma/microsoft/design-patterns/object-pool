﻿using System;
using System.Collections.Generic;

public abstract class Fruit
{
    public abstract void Eat();
}

public class Apple : Fruit
{
    public override void Eat()
    {
        Console.WriteLine("Eating an apple");
    }
}

public class Banana : Fruit
{
    public override void Eat()
    {
        Console.WriteLine("Eating a banana");
    }
}

public class FruitPool<T> where T : Fruit
{
    private Queue<T> pool = new Queue<T>();

    public FruitPool(int initialCapacity)
    {
        for (int i = 0; i < initialCapacity; i++)
        {
            pool.Enqueue(CreateNewFruit());
        }
    }

    public T Get()
    {
        if (pool.Count == 0)
        {
            pool.Enqueue(CreateNewFruit());
        }
        return pool.Dequeue();
    }

    public void Return(T fruit)
    {
        pool.Enqueue(fruit);
    }

    private T CreateNewFruit()
    {
        // You can implement custom logic to create a new fruit object here.
        // For simplicity, we're just creating a new instance of the generic type.
        return Activator.CreateInstance<T>();
    }
}

class Program
{
    static void Main(string[] args)
    {
        var fruitPool = new FruitPool<Apple>(5); // Initialize the pool with 5 apples

        var apple1 = fruitPool.Get();
        apple1.Eat();
        fruitPool.Return(apple1);

        var apple2 = fruitPool.Get();
        apple2.Eat();
        // ...

        var banana = new FruitPool<Banana>(3); // Initialize the pool with 3 bananas

        var banana1 = banana.Get();
        banana1.Eat();
        banana.Return(banana1);

        var banana2 = banana.Get();
        banana2.Eat();
        // ...

        // This demonstrates how objects can be reused from the pool.
    }
}
